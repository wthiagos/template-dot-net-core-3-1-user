//===============================================================================
//Web API Usuario
//
//===============================================================================
//Copyright (C) 2020-2020 
//Todos direitos reservados.
//Web API da entidade Usuario 
//==============================================================================

using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace UsuarioAPI.Entities
{
    [ExcludeFromCodeCoverage]
    [Table("UsuarioPerfil")]
    public class UsuarioPerfil
    {
        [Column("Id")]
        public int Id { get; set; }
        [Column("IdUsuario")]
        public int IdUsuario { get; set; }
        [Column("IdPerfil")]
        public int IdPerfil { get; set; }
        public virtual Usuario Usuario { get; set; }
        public virtual Perfil Perfil { get; set; }

    }
}