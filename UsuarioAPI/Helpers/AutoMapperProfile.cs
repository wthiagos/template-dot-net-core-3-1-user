//===============================================================================
//Web API Usuario
//
//===============================================================================
//Copyright (C) 2020-2020 
//Todos direitos reservados.
//Web API da entidade Usuario 
//==============================================================================

using System.Diagnostics.CodeAnalysis;
using System.Linq;
using AutoMapper;
using UsuarioAPI.Entities;
using UsuarioAPI.Models.Perfil;
using UsuarioAPI.Models.Usuario;

namespace UsuarioAPI.Helpers
{
    [ExcludeFromCodeCoverage]
    public class AutoMapperProfile : Profile
    {
        ///<summary>
        ///
        ///Esse método serve para mapear os objetos de entidades com suas models
        ///
        ///</summary>
        public AutoMapperProfile()
        {
            CreateMap<Usuario, UsuarioModel>()
            .ForMember(d => d.UsuarioPerfil,
                opt => opt.MapFrom(src => src.UsuarioPerfil.Select(p => p.Perfil))
            );
            CreateMap<UsuarioCreateModel, Usuario>();
            CreateMap<UsuarioUpdateModel, Usuario>();

            CreateMap<Perfil, PerfilModel>();
            CreateMap<PerfilCreateModel, UsuarioPerfil>();
            CreateMap<PerfilUpdateModel, UsuarioPerfil>();
        }
    }
}