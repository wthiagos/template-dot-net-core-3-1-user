//===============================================================================
//Web API Usuario
//
//===============================================================================
//Copyright (C) 2020-2020 
//Todos direitos reservados.
//Web API da entidade Usuario 
//==============================================================================

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace UsuarioAPI.Entities
{
    [ExcludeFromCodeCoverage]
    [Table("Usuario")]
    public class Usuario
    {
        [Column("Id")]
        public int Id { get; set; }
        [Column("Nome")]
        public string Nome { get; set; }
        [Column("Email")]
        public string Email { get; set; }
        [Column("DataCadastro")]
        public DateTime DataCadastro { get; set; }
        [Column("SenhaHash")]
        public byte[] SenhaHash { get; set; }
        [Column("SenhaSalt")]
        public byte[] SenhaSalt { get; set; }
        public List<UsuarioPerfil> UsuarioPerfil { get; set; }
    }
}