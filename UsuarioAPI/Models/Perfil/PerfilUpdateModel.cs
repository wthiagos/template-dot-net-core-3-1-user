//===============================================================================
//Web API Usuario
//
//===============================================================================
//Copyright (C) 2020-2020 
//Todos direitos reservados.
//Web API da entidade Usuario 
//==============================================================================

using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace UsuarioAPI.Models.Perfil
{
    [ExcludeFromCodeCoverage]
    public class PerfilUpdateModel
    {
        [Required]
        public int? IdPerfil { get; set; }
    }
}
