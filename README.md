# DotNet Core 3.1 Template Usuario

Template usando Dotnet Core para criação de usuários usando JWT token

## Início

Essas instruções servirão para te ajudar a clonar o projeto em sua maquina e executar o mesmo

### Pre-requisitos

Para rodar essa aplicação, primeiro precisara da SDK do Dotnet  Core instalado em sua máquina, seja Linux, Windows ou MacOs, para baixar, use o link abaixo

```
https://dotnet.microsoft.com/download
```

Após isso para visualizar o código, é preciso uma IDE, no meu caso usei o Visual Studio Code, para realizar o download do mesmo baixe no link abaixo 

```
https://code.visualstudio.com
```

Por fim, para a comunicação com o banco de dados, foi utilizado a versão develop do SQL Server, para fazer o download do mesmo use o link abaixo

```
https://www.microsoft.com/pt-br/sql-server/sql-server-downloads
```

### Instalando

Após instalar todos os programas necessários, o primeiro passo é clonar esse repositório, que pode ser feito através de 

```
git clone https://gitlab.com/wthiagos/template-dot-net-core-3-1-user.git
```

Após a finalização da clonagem do repositório, será necessário restaurar todos os packages do projeto, quando abrir a pasta do repositório com o Visual Studio Code, abra o console do mesmo e execute

```
dotnet restore
```

Após isso para executar o projeto execute

```
dotnet run
```
## Rodando os testes

Para executar os testes unitários dessa aplicação, sera necessário abrir a pasta do repositório na IDE e executar via console 

```
dotnet test /p:CollectCoverage=true
```

## Construído com

* [DotNet Core 3.1](https://docs.microsoft.com/pt-br/dotnet/core/) - Framework para backend
* [SQL Server 2019](https://www.microsoft.com/pt-br/sql-server/sql-server-2019) - Banco de dados
* [Entity Framework Core](https://docs.microsoft.com/pt-br/ef/core/) - ORM



## Autores

* **Thiago de Sa** - *Trabalho Inicial* - [wthiagos](https://gitlab.com/wthiagos)

Também a lista de [contribuidores](https://gitlab.com/wthiagos/https://gitlab.com/wthiagos/template-dot-net-core-3-1-user.git/-/graphs/master) que participaram deste projeto.
