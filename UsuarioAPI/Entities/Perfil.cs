//===============================================================================
//Web API Usuario
//
//===============================================================================
//Copyright (C) 2020-2020 
//Todos direitos reservados.
//Web API da entidade Usuario 
//==============================================================================

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace UsuarioAPI.Entities
{
    [ExcludeFromCodeCoverage]
    [Table("Perfil")]
    public class Perfil
    {
        [Column("Id")]
        public int Id { get; set; }
        [Column("Descricao")]
        public string Descricao { get; set; }
        public virtual List<UsuarioPerfil> UsuarioPerfil { get; set; }
    }
}