//===============================================================================
//Web API Usuario
//
//===============================================================================
//Copyright (C) 2020-2020 
//Todos direitos reservados.
//Web API da entidade Usuario 
//==============================================================================

using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using UsuarioAPI.Models.Perfil;

namespace UsuarioAPI.Models.Usuario
{
    [ExcludeFromCodeCoverage]
    public class UsuarioModel
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public List<PerfilModel> UsuarioPerfil { get; set; }
    }
}